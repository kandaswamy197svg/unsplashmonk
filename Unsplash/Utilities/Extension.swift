//
//  Extension.swift
//  Unsplash
//
//  Created by Kandavel on 01/05/19.
//  Copyright © 2019 Kandavel. All rights reserved.
//

import Foundation
import Foundation
import UIKit
extension JSONDecoder {
    static func jsonDecode() -> JSONDecoder {
        let decoder = JSONDecoder()
        if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
            decoder.dateDecodingStrategy = .iso8601
        }
        return decoder
    }
}
extension JSONEncoder {
    static func jsonEncode() -> JSONEncoder {
        let encoder = JSONEncoder()
        return encoder
    }
}



extension UIImageView {
    func setRemoteImage(urlString: String,defaultImage : UIImage){
        let imageCache = NSCache<NSString, AnyObject>()
        let url = URL(string: urlString)
        image = defaultImage
        if let imageFromCache = imageCache.object(forKey: urlString as NSString) as? UIImage {
            self.image = imageFromCache
            return
        }
        URLSession.shared.dataTask(with: url!) {
            data, response, error in
            if let response = data {
                DispatchQueue.main.async {
                    if  let imageToCache = UIImage(data: response) {
                        imageCache.setObject(imageToCache, forKey: urlString as NSString)
                        UIView.transition(with: self, duration: 0.25, options: [.transitionCrossDissolve], animations: {
                            self.image = imageToCache
                        }, completion: nil)
                    }
                    
                }
            }
            }.resume()
    }
}
