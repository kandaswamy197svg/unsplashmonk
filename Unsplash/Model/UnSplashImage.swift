//
//  RandomImage.swift
//  Unsplash
//
//  Created by Kandavel on 01/05/19.
//  Copyright © 2019 Kandavel. All rights reserved.
//

// To parse the JSON, add this file to your project and do:
//
//   let splashImage = try? newJSONDecoder().decode(SplashImage.self, from: jsonData)

import Foundation

class UnSplashImage: Codable {
    let id: String?
    let createdAt, updatedAt: Date?
    let width, height: Int?
    let color, description, altDescription: String?
    let urls: Urls?
    let likes: Int?
    let likedByUser: Bool?
    let tags : [Tag]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case width, height, color, description
        case altDescription = "alt_description"
        case urls,tags
        case likes
        case likedByUser = "liked_by_user"
    }
    
    init(id: String?=nil, createdAt: Date?=nil, updatedAt: Date?=nil, width: Int?=nil, height: Int?=nil, color: String?=nil, description: String?=nil, altDescription: String?=nil, urls: Urls?=nil, likes: Int?=nil, likedByUser: Bool?=nil,tags : [Tag]? = nil) {
        self.id = id
        self.createdAt = createdAt
        self.updatedAt = updatedAt
        self.width = width
        self.height = height
        self.color = color
        self.description = description
        self.altDescription = altDescription
        self.urls = urls
        self.likes = likes
        self.likedByUser = likedByUser
        self.tags  = tags
    }
}


class Tag : Codable {
    let title : String?
    init(title : String? = nil) {
        self.title  = title
    }
}


class Urls: Codable {
    let raw, full, regular, small: String?
    let thumb: String?
    
    init(raw: String?, full: String?, regular: String?, small: String?, thumb: String?) {
        self.raw = raw
        self.full = full
        self.regular = regular
        self.small = small
        self.thumb = thumb
    }
}


class UnsplashImageResult: Codable {
    let totalPages : Int?
    let results : [UnSplashImage]?
    let total : Int?
    enum CodingKeys: String, CodingKey {
       case totalPages = "total_pages"
       case results,total
    }
}
