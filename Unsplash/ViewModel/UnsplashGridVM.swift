//
//  UnsplashGridVM.swift
//  Unsplash
//
//  Created by Kandavel on 02/05/19.
//  Copyright © 2019 Kandavel. All rights reserved.
//

import Foundation
class  UnsplashGridVM {
  
    weak var unsplashImageDelegate : UnsplashImageDelegate?
    
    
    init(delegate : UnsplashImageDelegate) {
       unsplashImageDelegate = delegate
    }
    
    func getListOfImages(queryType : String? = nil) {
        NetworkManager.shared.getSearchImages(userInput: queryType ?? "random", completion: resultsImagesResponsehandler)
    }
    
    func resultsImagesResponsehandler(listOFimageResponse :Any?,response :URLResponse?,error :Error?) {
        var unsplashImageList : [UnSplashImage] = []
        guard let listOFimagesData = listOFimageResponse as? UnsplashImageResult else {
            return
        }
        unsplashImageList = getUnsplashImagesFromImageResult(imageResult: listOFimagesData)
        unsplashImageDelegate?.getListOfImages(images: unsplashImageList)
    }
    
    func getUnsplashImagesFromImageResult(imageResult : UnsplashImageResult) -> [UnSplashImage]  {
        return imageResult.results ?? []
    }
    
    
    
    
}
