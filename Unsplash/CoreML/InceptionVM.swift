//
//  InceptionModel.swift
//  Unsplash
//
//  Created by Kandavel on 05/05/19.
//  Copyright © 2019 Kandavel. All rights reserved.
//

import Foundation
import CoreML
import UIKit
class InceptionVM {
    
    var model: Inceptionv3!
    var currentContextImage : UIImage!
    weak var inceptionV3delegate : Inceptionv3Delegate?
    init(delegate : Inceptionv3Delegate) {
        self.inceptionV3delegate  = delegate
    }
    
    func configureModel() {
       model = Inceptionv3()
    }
    
    
    //change The Selected Image to Required Frame
    // 299 * 299
    func changeTheImageFrame(image : UIImage)  {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 299, height: 299), true, 2.0)
        image.draw(in: CGRect(x: 0, y: 0, width: 299, height: 299))
        currentContextImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        convertImageToPixel(image: currentContextImage)
    }
    
    //store  the pixel in Main-Memory
    func convertImageToPixel(image : UIImage) {
        let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue, kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
        var pixelBuffer : CVPixelBuffer?
        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(currentContextImage!.size.width), Int(currentContextImage!.size.height), kCVPixelFormatType_32ARGB, attrs, &pixelBuffer)
        guard (status == kCVReturnSuccess) else {
            return
        }
        
        CVPixelBufferLockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer!)
        
        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
        let context = CGContext(data: pixelData, width: Int(currentContextImage!.size.width), height: Int(currentContextImage!.size.height), bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer!), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue) //3
        
        context?.translateBy(x: 0, y: currentContextImage!.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        
        //give the new  rendered image with new fram
        UIGraphicsPushContext(context!)
        currentContextImage!.draw(in: CGRect(x: 0, y: 0, width: currentContextImage!.size.width, height: currentContextImage!.size.height))
        UIGraphicsPopContext()
        CVPixelBufferUnlockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        
        predictObjectInTheSelectedImage(pixelBuffer: pixelBuffer)
    }
    
    func predictObjectInTheSelectedImage(pixelBuffer : CVPixelBuffer?) {
        guard let prediction = try? model.prediction(image: pixelBuffer!) else {
             self.inceptionV3delegate?.getPredicatedMessageFromInceptionModel(text: nil)
            return
        }
       
        self.inceptionV3delegate?.getPredicatedMessageFromInceptionModel(text: prediction.classLabel)
        
        
    }
    
    
    }
    
    
    
    

