//
//  Protocols.swift
//  Unsplash
//
//  Created by Kandavel on 02/05/19.
//  Copyright © 2019 Kandavel. All rights reserved.
//

import Foundation
import UIKit
protocol UnsplashImageDelegate : class {
    func getListOfImages(images : [UnSplashImage])
}
extension UnsplashImageDelegate {
    func getListOfImages(images : [UnSplashImage]) {
      //leaves Empty Here
    }
    
}
protocol Inceptionv3Delegate : class {
    func getPredicatedMessageFromInceptionModel(text : String?)
}
protocol GridCollectionViewDelegate : class {
    func getSelectedImage(image : UIImage)
}
extension GridCollectionViewDelegate {
    func getSelectedImage(image : UIImage) {
       //leaves Empty Here
    }
    
}
