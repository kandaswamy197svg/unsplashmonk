//
//  CollectionViewCell.swift
//  Unsplash
//
//  Created by Kandavel on 01/05/19.
//  Copyright © 2019 Kandavel. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    
    //MARK:Iboutlet
    @IBOutlet weak var unsplashView: UIImageView!
    @IBOutlet weak var imagenameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
    
    func configureUI() {
      unsplashView.layer.cornerRadius = 4
    }
    
    func configureData(image : UnSplashImage) {
     unsplashView.setRemoteImage(urlString: image.urls?.regular ?? "", defaultImage: UIImage())
     imagenameLabel.text  = image.tags?.randomElement()?.title ?? ""
    }
}
