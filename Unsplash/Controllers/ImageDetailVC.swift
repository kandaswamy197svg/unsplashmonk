//
//  ImageDetailVC.swift
//  Unsplash
//
//  Created by Kandavel on 05/05/19.
//  Copyright © 2019 Kandavel. All rights reserved.
//

import UIKit
import CoreML

class ImageDetailVC: UIViewController {
    
    //MARK:IbOutlets
    @IBOutlet weak var spllashImageView: UIImageView!
    @IBOutlet weak var predictedTextLabel: UILabel!
    //MARK: Variables
    var image : UIImage!
    var inceptionV3VM : InceptionVM!
    
    class func  openImageDetailVC(image : UIImage) ->  ImageDetailVC {
      let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: String(describing: ImageDetailVC.self)) as! ImageDetailVC
        vc.image  = image
        return vc
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spllashImageView.image = image
        predictedTextLabel.text  = "Analyzing Image..."
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        inceptionV3VM = InceptionVM(delegate: self)
        inceptionV3VM.configureModel()
        inceptionV3VM.changeTheImageFrame(image: image)
        
    }
    
    
}
extension ImageDetailVC : Inceptionv3Delegate {
    
    func getPredicatedMessageFromInceptionModel(text: String?) {
        guard let predictedText =  text else {
            predictedTextLabel.text =  "Unbale To detect Object in the Image"
            return
            
        }
        predictedTextLabel.text = "This is a \(predictedText)"
    }
    
}
