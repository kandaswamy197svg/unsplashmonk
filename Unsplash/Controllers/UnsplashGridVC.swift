//
//  ViewController.swift
//  Unsplash
//
//  Created by Kandavel on 01/05/19.
//  Copyright © 2019 Kandavel. All rights reserved.
//

import UIKit

class UnsplashGridVC: UIViewController {

    
    //MARK:IBOUTLET
    @IBOutlet weak var collectionView: UICollectionView!
    //MARK:Varaiable
    var unsplashGridVM : UnsplashGridVM!
    var gridCollection : GridCollectionView!
    
    private lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Search photos"
        searchController.searchBar.autocapitalizationType = .none
        return searchController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSearchController()
        unsplashGridVM = UnsplashGridVM(delegate: self)
        unsplashGridVM.getListOfImages(queryType: nil)
        gridCollection =  GridCollectionView(collectionView: self.collectionView, delegate: self)
        collectionView.dataSource = gridCollection
        collectionView.delegate   = gridCollection
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.itemSize = CGSize(width: 100, height: 100)
            flowLayout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20);
            flowLayout.scrollDirection  = .vertical
            flowLayout.minimumInteritemSpacing = 10.0
        }
        collectionView.reloadData()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        searchController.dismiss(animated: false, completion: nil)
    }
    
    
    
    func setUpSearchController() {
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
        extendedLayoutIncludesOpaqueBars = true
    }
    

}

// MARK: - UISearchBarDelegate
extension UnsplashGridVC: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else { return }
        unsplashGridVM.getListOfImages(queryType: text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        
        
    }
}
extension UnsplashGridVC :UnsplashImageDelegate {
    func getListOfImages(images: [UnSplashImage]) {
        DispatchQueue.main.async {
            self.gridCollection.reloadData(images: images)
        }
    }
}
extension UnsplashGridVC : GridCollectionViewDelegate {
    func getSelectedImage(image: UIImage) {
     let vc = ImageDetailVC.openImageDetailVC(image: image)
     self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
