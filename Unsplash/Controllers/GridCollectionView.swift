//
//  Collection.swift
//  Unsplash
//
//  Created by Kandavel on 01/05/19.
//  Copyright © 2019 Kandavel. All rights reserved.
//

import Foundation
import UIKit
class GridCollectionView : NSObject ,UICollectionViewDataSource, UICollectionViewDelegate {
   
    
    //MARK:Variable
    var collectionView : UICollectionView!
    var cellIdentifier = String(describing: CollectionViewCell.self)
    var unsplasImages : [UnSplashImage]  = []
    weak var gridCollectionViewDelegate : GridCollectionViewDelegate?
    
    init(collectionView : UICollectionView, delegate : GridCollectionViewDelegate) {
        super.init()
        self.collectionView  = collectionView
        self.gridCollectionViewDelegate = delegate
        self.collectionView.register(UINib(nibName: String(describing: CollectionViewCell.self), bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        
    }
    
    func reloadData(images : [UnSplashImage]) {
        unsplasImages  = images
        self.collectionView.reloadData()
        if !(unsplasImages.isEmpty) {
            self.collectionView.performBatchUpdates({
                let indexSet = IndexSet(integersIn: 0...0)
                self.collectionView.reloadSections(indexSet)
            }, completion: nil)
        }
       
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  unsplasImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      let cell =   collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? CollectionViewCell
        let unsplashImage  = unsplasImages[indexPath.row]
        cell?.configureData(image: unsplashImage)
        return cell ?? UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      let cell  = collectionView.cellForItem(at: indexPath) as? CollectionViewCell
        guard let image  = cell?.unsplashView.image  else {
            return
        }
        gridCollectionViewDelegate?.getSelectedImage(image: image)
        
    }
    
    
    
    
}
