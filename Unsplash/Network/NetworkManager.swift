//
//  NetworkManager.swift
//  Go-Contact
//
//  Created by Kandavel on 17/03/19.
//  Copyright © 2019 Kandavel. All rights reserved.
//

import Foundation
class NetworkManager {
    static let shared: NetworkManager = NetworkManager()
    let sharedSession = URLSession.shared
    typealias responseCompletion = (Any?,URLResponse?,Error?) -> Void
    
    init() {
        
    }
    
    func getSearchImages(userInput : String ,completion: @escaping responseCompletion) {
        let contactURL =  Constants.baseURL + "search/photos?page=1&client_id=d5c91d8f55769c8ff9abd0617313091472a3cfa6191747d88f883e486fcf6c2d&query=\(userInput)"
        if let url = URL(string: contactURL) {
            var request = URLRequest(url: url)
           request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod  = "GET"
            
            DispatchQueue.global(qos: .userInitiated).async {
                let dataTask = self.sharedSession.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                    if let jsonData  =  data {
                        guard let unsplashImageResult = try? JSONDecoder.jsonDecode().decode(UnsplashImageResult.self, from: jsonData)  else {
                            completion(nil,response,error)
                            return
                        }
                        print(unsplashImageResult)
                        completion(unsplashImageResult,response,error)
                    }
                    else {
                        completion(nil,response,error)
                    }
                })
                dataTask.resume()
            }
           
        }
    }
}
